<?php

$config = ['app' => ['name' => 'ly-admin',//应用唯一辨识名称,分布式下会有用
    'debug' => false,       //debug模式
    'init' => \app\AppInit::class,//初始化类
],
    //拦截器
    'interceptors' => [\app\interceptors\RapInterceptors::class],
    'cache'=>['type'=>'file'],
    'log'=>['type'=>'file']
];
$config['admin_proxy'] = include "dev.php";
return $config;