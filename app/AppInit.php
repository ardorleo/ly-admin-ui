<?php
namespace app;

use app\controller\HomeController;
use rap\cache\Cache;
use rap\Init;
use rap\web\mvc\AutoFindHandlerMapping;
use rap\web\mvc\Router;
use rap\web\mvc\RouterGroup;

/**
 * User: jinghao@duohuo.net
 * Date: 18/8/31
 * Time: 下午4:39
 * Link:  http://magapp.cc
 * Copyright:南京灵衍信息科技有限公司
 */
class AppInit implements Init {


    public function appInit(AutoFindHandlerMapping $autoMapping, Router $router) {
        $router->index(function (){
            return redirect('/portal/start/install');
        });
        //入口方法
        $router->group('portal')->then(function(RouterGroup $routerGroup){
            $routerGroup->when('login')->toDo(function (){
                return body(file_get_contents(ROOT_PATH.'/portal/login.html'));
            });
            $routerGroup ->whenMiss(function() {
                if(request()->ext()=='html'){
                    response()->code(404);
                    return body('');
                }
                return body(file_get_contents(ROOT_PATH.'/portal/index.html'));
            });
        });

    }

}