Rap.define('/ly/utils/directive.js', [], function () {
    Vue.directive('table-affix', {
        inserted: function (el, binding, vnode) {
            if (el.className.indexOf('el-table') == -1 && el.className.indexOf('ly-flex-table') == -1) {
                return;
            }
            var table = el;

            var scrollListener = function () {
                //小屏幕的 table 的自适应 不需要锁定
                if (window.innerWidth < 600 && table.className.indexOf('table-response') > -1) {
                    return;
                }
                var y = table.getBoundingClientRect().y;
                var height = table.getBoundingClientRect().height;
                var hasCls = table.className.indexOf('table-fix-header') > -1;

                if (y < 56 && !hasCls && height > 100 && y + height > 90) {
                    table.className += " table-fix-header";
                    var header = table.getElementsByClassName('el-table__header-wrapper')[0];
                    if (header) {
                        header.style.width = table.clientWidth + "px";
                    } else {
                        header = table.getElementsByClassName('ly-table-header')[0];
                        if (header) {
                            header.style.width = table.clientWidth + "px";
                        }
                    }

                } else if (y > 56 && hasCls || y + height < 90) {
                    table.className = table.className.replace('table-fix-header', '').trim();
                }
                var pagination = table.nextElementSibling;
                if (!pagination || pagination.className.indexOf('pagination') == -1) {
                    return;
                }
                if (y + 93 <= window.innerHeight && y + table.clientHeight > window.innerHeight - 53) {
                    var fitBottom = pagination.className.indexOf('fixed-bottom') > -1;
                    if (!fitBottom) {
                        pagination.className += " fixed-bottom";
                    }
                } else {
                    pagination.className = pagination.className.replace('fixed-bottom', '').trim();
                }
            };
            vnode.componentInstance.$watch('data.length', scrollListener);
            this.scrollListener = scrollListener;
            window.addEventListener('scroll', scrollListener);
        }, unbind: function (el) {
            if (el.className.indexOf('el-table') == -1) {
                return true;
            }
            window.removeEventListener('scroll', this.scrollListener);
        }
    });

    //文本按行省略
    Vue.directive('clamp', {
        inserted: function (el, binding, vnode) {
            el.classList.add('clamp');
            if (binding.expression) {
                el.style['-webkit-line-clamp'] = binding.expression;
            }
        }
    });
    //margin-top
    Vue.directive('mt', {
        inserted: function (el, binding, vnode) {
            var margin = 10;
            if (binding.expression) {
                margin = parseInt(binding.expression);
            }
            el.style['marginTop'] = margin + 'px';
        }
    });
    //margin-bottom
    Vue.directive('mb', {
        inserted: function (el, binding, vnode) {
            var margin = 10;
            if (binding.expression) {
                margin = parseInt(binding.expression);
            }
            el.style['marginBottom'] = margin + 'px';
        }
    });
    //padding-top
    Vue.directive('pt', {
        inserted: function (el, binding, vnode) {
            var margin = 10;
            if (binding.expression) {
                margin = parseInt(binding.expression);
            }
            el.style['paddingTop'] = margin + 'px';
        }
    });
    //padding-bottom
    Vue.directive('pb', {
        inserted: function (el, binding, vnode) {
            var margin = 10;
            if (binding.expression) {
                margin = parseInt(binding.expression);
            }
            el.style['paddingBottom'] = margin + 'px';
        }
    });



});