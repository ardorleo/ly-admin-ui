/**
 * 权限管理
 */
Rap.define('/ly/utils/role.js', [], function () {
    var config = {
        role: '',
        super_role: 'super',
        resources: []
    };

    function checkElRole (el, binding, vnode) {
        function show(el) {
            var bef = el.attributes.displaybef;
            if (bef) {
                el.style.display = bef;
            }else{
                el.style.display = "";
            }
        }
        var role_key = binding.rawName.replace('v-role:', '').replace('.$f', '').replace('.$h', '');
        var arg = binding.arg;
        if (!arg) {
            arg = binding.value;
        }
        if (config.role === config.super_role) {
            show(el);
            return;
        }
        if (!arg) {

            //没有设置action
            if (binding.modifiers && binding.modifiers[config.role]) {
                show(el);

                return;
            }
        } else {
            if (role_key === 'v-role') {
                role_key = arg;
            }
            if (config.resources.indexOf(role_key) > -1) {
                show(el);
                return;
            }
        }
        if(el.style.display!=='none'){
            el.attributes.displaybef = el.style.display;
        }
        el.style.display = 'none';

    }

    //v-role:a.b.c 资源校验
    //v-role="'a.b.c'" 资源校验
    //v-role.role1.role2 校验权限
    Vue.directive('role', {
        bind:checkElRole,
        insert:checkElRole,
        update:checkElRole
    });
    var _menu_map = {};

    function menuMap(menus, category) {
        for (var i = 0; i < menus.length; i++) {
            var menu = menus[i];
            menu.category = category;
            var link = menu.link;
            if (menu.link) {
                _menu_map[link] = menu;
                if (menu.links) {
                    for (var j = 0; j < menu.links.length; j++) {
                        link = menu.links[j];
                        _menu_map[link] = menu;
                    }
                }
                var category_code = 'category.' + category;
                if (menu.code && (config.resources.indexOf(menu.code) > -1) && config.resources.indexOf(category_code) === -1) {
                    config.resources.push(category_code);
                }
            }
            if (menu.children) {
                menuMap(menu.children, category);
            }
        }
    }

    var _menuData = [];
    return {
        /**
         *  检查是否为超级管理员
         */
        isSuper: function () {
            return config.role === config.super_role
        },
        menus: function (menus_all) {
            if (!menus_all) {
                return _menuData;
            }
            _menuData = menus_all;
            _menu_map = {};
            for (var i = 0; i < menus_all.length; i++) {
                var menu = menus_all[i];
                var category = menu.category;
                menuMap(menu.menus, category);
            }
            var categoryNum = 0;
            for (var i = 0; i < menus_all.length; i++) {
                var menu = menus_all[i];
                var category = menu.category;
                var category_code = 'category.' + category;
                if (this.checkResource(category_code)) {
                    categoryNum += 1;
                }
            }
            if (categoryNum > 1) {
                config.resources.push('category');
            }
        },
        toMenuCategory: function (category) {
            for (var k in _menu_map) {
                var menu = _menu_map[k];
                if (category === menu.category) {
                    if (this.checkResource(menu.code)) {
                        Rap.navigateTo(menu.link);
                        return;
                    }
                }
            }
        },
        /**
         *  配置
         * @param set
         */
        config: function (set) {
            if(!set){
                return config;
            }
            _.extend(config, set);
            config.role = "" + config.role;
            if (config.menus) {
                this.menus(config.menus);
            }
            return config;
        },
        /**
         * 检查是否拥有角色
         * @param role  角色 支持 数组或字符串
         * @returns {boolean}
         */
        checkRole: function (role) {
            if (_.isArray(role)) {
                return role.indexOf(config.role) > -1;
            } else {
                return role === config.role;
            }
        },

        checkPage: function () {
            var routerUrl = Rap.routerUrl();
            if (routerUrl.indexOf('?') > 0) {
                routerUrl = routerUrl.substring(0, routerUrl.indexOf('?'))
            }
            var menu = _menu_map[routerUrl];
            if (menu && menu.code) {
                return this.checkResource(menu.code);
            }
            return true;

        },
        //检查是否超管
        checkSuper: function (role) {
            return this.checkRole(config.super_role);
        },
        superRole: function (role) {
            return config.super_role;
        },
        /**
         * 检查是否拥有资源
         * @param resource 资源 支持数组或字符串
         * @returns {boolean}
         */
        checkResource: function (resource) {
            if (this.checkSuper()) {
                return true;
            }
            if(!resource){
                return true;
            }
            function check(res) {
                return config.resources.indexOf(res) > -1;
            }

            if (_.isArray(resource)) {
                for (var i = 0; i < resource.length; i++) {
                    var success = check(resource[i]);
                    if (success) {
                        return true;
                    }
                }
                return false;
            } else {
                return check(resource);
            }

        }
    };
});
