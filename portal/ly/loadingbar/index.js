Rap.define('', [], function () {
    var LoadingBarConstructor=Vue.component('ly-Loading-bar');
    var timer = null;
    var removeTimer = null;
    var LyLoadingBar = function () {
        return new LoadingBarConstructor()
    };

    LoadingBarConstructor.prototype.config = function (options) {
        Object.keys(options).forEach(function (key) {
            if (key === 'isError' || key === 'totalProgress') {
                return;
            }
            this[key] = options[key];
        })
    };

    LoadingBarConstructor.prototype.init = function () {
        clearTimeout(timer);
        this.totalProgress = 0;
        this.isError = false;
        this.vm = this.$mount();
        document.body.appendChild(this.vm.$el);
        return this
    };

    LoadingBarConstructor.prototype.start = function () {
        this.init();
        var me = this;
        timer = setInterval(function () {
            if (me.totalProgress < 90) {
                me.totalProgress += (me.percentNum || Math.random()) * me.speed
            }
        }, 100)
    };

    LoadingBarConstructor.prototype.end = function () {
        timer || this.init();
        this.totalProgress = 100;
        var me = this;
        clearTimeout(removeTimer);
        removeTimer = setTimeout(function () {
            clearTimeout(timer);
            timer = null;
            document.body.removeChild(me.vm.$el)
        }, 200);
    };

    LoadingBarConstructor.prototype.error = function () {
        this.end();
        this.totalProgress = 80;
        this.isError = true;
    };
    Vue.prototype.$loadingbar = LyLoadingBar();
});