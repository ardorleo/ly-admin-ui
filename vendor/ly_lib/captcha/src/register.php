<?php
use rap\web\mvc\AutoFindHandlerMapping;
use rap\web\mvc\Router;
use rap\aop\Event;
use lylib\captcha\CaptchaController;
//判定是 Rap环境
Event::add(\rap\ServerEvent::ON_APP_INIT, function(AutoFindHandlerMapping $autoMapping, Router $router) {
    $autoMapping->prefix('/captcha', CaptchaController::class);
});
