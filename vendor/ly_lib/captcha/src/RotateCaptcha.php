<?php

namespace lylib\captcha;

use rap\cache\Cache;

class RotateCaptcha
{


    public function check($verify_code,$_sid)
    {
        $cache_key = md5(request()->session()->sessionId() . ':RotateCaptcha'.$_sid);
        $item = Cache::get($cache_key);
        Cache::remove($cache_key);
        $sign = $item[0];
        $angle = $item[1];
        $item_md5 = $item[2];
        $code = EncryptUtil::decrypt($verify_code, $sign);
        $item_md51 = substr($code, 0, 32);
        $time = ((int)substr($code, 32, 13)) / 1000;
        $angle1 = substr($code, 45);
        if ($item_md5 == $item_md51 && abs(time() - $time) < 30 && (abs($angle1 - $angle) < 5||abs($angle1/2 - $angle) < 5)) {
            return true;
        }
        return false;
    }

    /**
     * 构造图片
     * @param $srcFile
     * @return false|GdImage|resource
     */
    private function createImg($srcFile)
    {
        $data = getimagesize($srcFile);
        switch ($data['2']) {
            case 1:
                $im = imagecreatefromgif($srcFile);
                break;
            case 2:
                $im = imagecreatefromjpeg($srcFile);
                break;
            case 3:
                $im = imagecreatefrompng($srcFile);
                break;
            case 6:
                $im = imagecreatefromwbmp($srcFile);
                break;
        }
        return $im;
    }

    /**
     * 在图片上截取一个正方形
     * @param $uimg
     * @param $x
     * @param $y
     * @param $width
     * @param $height
     * @return false|GdImage|resource
     */
    private function crop($uimg, $width, $height,$angle)
    {
        $uimg_r=imagerotate($uimg, $angle, 0);
        $x = (imagesx($uimg_r) - $width) / 2;
        $y = (imagesy($uimg_r) - $height) / 2;
        $new_image = imagecreatetruecolor($width, $height);
        imagecopyresampled($new_image, $uimg_r, 0, 0, $x, $y, $width, $height, $width, $height);
        imagedestroy($uimg_r);
        return $new_image;
    }

    function circleImage($userHeadInfo, $width, $height)
    {
        $tempWidth =$width;
        $tempHeight =$height;
        $w = $tempWidth;
        $h = $tempHeight;
        $w = min($w, $h);
        $h = $w;
        //生成一张与模板底图一样大小的画布
        $newImg = imagecreatetruecolor($tempWidth, $tempHeight);
        //分配颜色 + alpha，将颜色填充到新图上
        $alpha = imagecolorallocatealpha($newImg, 0, 0, 0, 127);
        imagefill($newImg, 0, 0, $alpha);
        $r = $w / 2; //圆半径
        for ($x = 0; $x < $w; $x++) {
            for ($y = 0; $y < $h; $y++) {
                $rgbColor = imagecolorat($userHeadInfo, $x, $y);
                if (((($x - $r) * ($x - $r) + ($y - $r) * ($y - $r)) < ($r * $r))) {
                    imagesetpixel($newImg, $x, $y, $rgbColor);
                }
            }
        }
        imagesavealpha($newImg, true);
        return $newImg;
    }


    public function cropItem($bg_url,$angle)
    {
        $dir = __DIR__ . '/assets/slide/';
        $uimg = $this->createImg($bg_url);
        $img = $this->crop($uimg, 90, 90,$angle);
        $img = $this->circleImage($img,90, 90);
        ob_start();
        imagepng($img);
        $content = ob_get_clean();
        imagedestroy($img);
        imagedestroy($uimg);
        return $content;
    }


    public function build($_sid=1)
    {
        $dir = __DIR__ . '/assets/slide/';
        $bg = rand(1, 6);
        $bg_url = $dir . 'bg/bg' . $bg . '.png';
        $angle=rand(40,340);
        $item = $this->cropItem($bg_url,$angle);
        $background = file_get_contents($bg_url);
        $item = 'data:image/png;base64,' . chunk_split(base64_encode($item));
        $background = 'data:image/png;base64,' . chunk_split(base64_encode($background));
        $sign = md5(time());
        $item_md5 = md5($item);
        $item = EncryptUtil::encrypt($item, $sign);
        $background = EncryptUtil::encrypt($background, $sign);
        $code = substr($sign, 0, 16) . $background . ' ' . substr($sign, 16) . $item . ' ' . $item_md5;
        $cache_key=md5(request()->session()->sessionId() . ':RotateCaptcha'.$_sid);
        Cache::set($cache_key, [$sign, $angle, $item_md5], 600);
        return ['code' => $code];
    }

}