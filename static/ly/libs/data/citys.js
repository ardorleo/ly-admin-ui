window.cityChina = [
    {
        id: 1,
        name: "北京市",
        parent_area_key: "",
        area_key: "110000",
        rank: "1",
        _child: [
            {
                id: 2,
                name: "市辖区",
                parent_area_key: "110000",
                area_key: "110100",
                rank: "2"
            }
        ]
    },
    {
        id: 19,
        name: "天津市",
        parent_area_key: "",
        area_key: "120000",
        rank: "19",
        _child: [
            {
                id: 20,
                name: "市辖区",
                parent_area_key: "120000",
                area_key: "120100",
                rank: "20"
            }
        ]
    },
    {
        id: 37,
        name: "河北省",
        parent_area_key: "",
        area_key: "130000",
        rank: "37",
        _child: [
            {
                id: 38,
                name: "石家庄市",
                parent_area_key: "130000",
                area_key: "130100",
                rank: "38"
            },
            {
                id: 61,
                name: "唐山市",
                parent_area_key: "130000",
                area_key: "130200",
                rank: "61"
            },
            {
                id: 77,
                name: "秦皇岛市",
                parent_area_key: "130000",
                area_key: "130300",
                rank: "77"
            },
            {
                id: 86,
                name: "邯郸市",
                parent_area_key: "130000",
                area_key: "130400",
                rank: "86"
            },
            {
                id: 107,
                name: "邢台市",
                parent_area_key: "130000",
                area_key: "130500",
                rank: "107"
            },
            {
                id: 128,
                name: "保定市",
                parent_area_key: "130000",
                area_key: "130600",
                rank: "128"
            },
            {
                id: 153,
                name: "张家口市",
                parent_area_key: "130000",
                area_key: "130700",
                rank: "153"
            },
            {
                id: 171,
                name: "承德市",
                parent_area_key: "130000",
                area_key: "130800",
                rank: "171"
            },
            {
                id: 184,
                name: "沧州市",
                parent_area_key: "130000",
                area_key: "130900",
                rank: "184"
            },
            {
                id: 202,
                name: "廊坊市",
                parent_area_key: "130000",
                area_key: "131000",
                rank: "202"
            },
            {
                id: 214,
                name: "衡水市",
                parent_area_key: "130000",
                area_key: "131100",
                rank: "214"
            },
            {
                id: 227,
                name: "省直辖县级行政区划",
                parent_area_key: "130000",
                area_key: "139000",
                rank: "227"
            }
        ]
    },
    {
        id: 230,
        name: "山西省",
        parent_area_key: "",
        area_key: "140000",
        rank: "230",
        _child: [
            {
                id: 231,
                name: "太原市",
                parent_area_key: "140000",
                area_key: "140100",
                rank: "231"
            },
            {
                id: 243,
                name: "大同市",
                parent_area_key: "140000",
                area_key: "140200",
                rank: "243"
            },
            {
                id: 256,
                name: "阳泉市",
                parent_area_key: "140000",
                area_key: "140300",
                rank: "256"
            },
            {
                id: 263,
                name: "长治市",
                parent_area_key: "140000",
                area_key: "140400",
                rank: "263"
            },
            {
                id: 278,
                name: "晋城市",
                parent_area_key: "140000",
                area_key: "140500",
                rank: "278"
            },
            {
                id: 286,
                name: "朔州市",
                parent_area_key: "140000",
                area_key: "140600",
                rank: "286"
            },
            {
                id: 294,
                name: "晋中市",
                parent_area_key: "140000",
                area_key: "140700",
                rank: "294"
            },
            {
                id: 307,
                name: "运城市",
                parent_area_key: "140000",
                area_key: "140800",
                rank: "307"
            },
            {
                id: 322,
                name: "忻州市",
                parent_area_key: "140000",
                area_key: "140900",
                rank: "322"
            },
            {
                id: 338,
                name: "临汾市",
                parent_area_key: "140000",
                area_key: "141000",
                rank: "338"
            },
            {
                id: 357,
                name: "吕梁市",
                parent_area_key: "140000",
                area_key: "141100",
                rank: "357"
            }
        ]
    },
    {
        id: 372,
        name: "内蒙古自治区",
        parent_area_key: "",
        area_key: "150000",
        rank: "372",
        _child: [
            {
                id: 373,
                name: "呼和浩特市",
                parent_area_key: "150000",
                area_key: "150100",
                rank: "373"
            },
            {
                id: 384,
                name: "包头市",
                parent_area_key: "150000",
                area_key: "150200",
                rank: "384"
            },
            {
                id: 395,
                name: "乌海市",
                parent_area_key: "150000",
                area_key: "150300",
                rank: "395"
            },
            {
                id: 400,
                name: "赤峰市",
                parent_area_key: "150000",
                area_key: "150400",
                rank: "400"
            },
            {
                id: 414,
                name: "通辽市",
                parent_area_key: "150000",
                area_key: "150500",
                rank: "414"
            },
            {
                id: 424,
                name: "鄂尔多斯市",
                parent_area_key: "150000",
                area_key: "150600",
                rank: "424"
            },
            {
                id: 435,
                name: "呼伦贝尔市",
                parent_area_key: "150000",
                area_key: "150700",
                rank: "435"
            },
            {
                id: 451,
                name: "巴彦淖尔市",
                parent_area_key: "150000",
                area_key: "150800",
                rank: "451"
            },
            {
                id: 460,
                name: "乌兰察布市",
                parent_area_key: "150000",
                area_key: "150900",
                rank: "460"
            },
            {
                id: 473,
                name: "兴安盟",
                parent_area_key: "150000",
                area_key: "152200",
                rank: "473"
            },
            {
                id: 480,
                name: "锡林郭勒盟",
                parent_area_key: "150000",
                area_key: "152500",
                rank: "480"
            },
            {
                id: 493,
                name: "阿拉善盟",
                parent_area_key: "150000",
                area_key: "152900",
                rank: "493"
            }
        ]
    },
    {
        id: 497,
        name: "辽宁省",
        parent_area_key: "",
        area_key: "210000",
        rank: "497",
        _child: [
            {
                id: 498,
                name: "沈阳市",
                parent_area_key: "210000",
                area_key: "210100",
                rank: "498"
            },
            {
                id: 513,
                name: "大连市",
                parent_area_key: "210000",
                area_key: "210200",
                rank: "513"
            },
            {
                id: 525,
                name: "鞍山市",
                parent_area_key: "210000",
                area_key: "210300",
                rank: "525"
            },
            {
                id: 534,
                name: "抚顺市",
                parent_area_key: "210000",
                area_key: "210400",
                rank: "534"
            },
            {
                id: 543,
                name: "本溪市",
                parent_area_key: "210000",
                area_key: "210500",
                rank: "543"
            },
            {
                id: 551,
                name: "丹东市",
                parent_area_key: "210000",
                area_key: "210600",
                rank: "551"
            },
            {
                id: 559,
                name: "锦州市",
                parent_area_key: "210000",
                area_key: "210700",
                rank: "559"
            },
            {
                id: 568,
                name: "营口市",
                parent_area_key: "210000",
                area_key: "210800",
                rank: "568"
            },
            {
                id: 576,
                name: "阜新市",
                parent_area_key: "210000",
                area_key: "210900",
                rank: "576"
            },
            {
                id: 585,
                name: "辽阳市",
                parent_area_key: "210000",
                area_key: "211000",
                rank: "585"
            },
            {
                id: 594,
                name: "盘锦市",
                parent_area_key: "210000",
                area_key: "211100",
                rank: "594"
            },
            {
                id: 600,
                name: "铁岭市",
                parent_area_key: "210000",
                area_key: "211200",
                rank: "600"
            },
            {
                id: 609,
                name: "朝阳市",
                parent_area_key: "210000",
                area_key: "211300",
                rank: "609"
            },
            {
                id: 618,
                name: "葫芦岛市",
                parent_area_key: "210000",
                area_key: "211400",
                rank: "618"
            }
        ]
    },
    {
        id: 626,
        name: "吉林省",
        parent_area_key: "",
        area_key: "220000",
        rank: "626",
        _child: [
            {
                id: 627,
                name: "长春市",
                parent_area_key: "220000",
                area_key: "220100",
                rank: "627"
            },
            {
                id: 639,
                name: "吉林市",
                parent_area_key: "220000",
                area_key: "220200",
                rank: "639"
            },
            {
                id: 650,
                name: "四平市",
                parent_area_key: "220000",
                area_key: "220300",
                rank: "650"
            },
            {
                id: 658,
                name: "辽源市",
                parent_area_key: "220000",
                area_key: "220400",
                rank: "658"
            },
            {
                id: 664,
                name: "通化市",
                parent_area_key: "220000",
                area_key: "220500",
                rank: "664"
            },
            {
                id: 673,
                name: "白山市",
                parent_area_key: "220000",
                area_key: "220600",
                rank: "673"
            },
            {
                id: 681,
                name: "松原市",
                parent_area_key: "220000",
                area_key: "220700",
                rank: "681"
            },
            {
                id: 688,
                name: "白城市",
                parent_area_key: "220000",
                area_key: "220800",
                rank: "688"
            },
            {
                id: 695,
                name: "延边朝鲜族自治州",
                parent_area_key: "220000",
                area_key: "222400",
                rank: "695"
            }
        ]
    },
    {
        id: 704,
        name: "黑龙江省",
        parent_area_key: "",
        area_key: "230000",
        rank: "704",
        _child: [
            {
                id: 705,
                name: "哈尔滨市",
                parent_area_key: "230000",
                area_key: "230100",
                rank: "705"
            },
            {
                id: 725,
                name: "齐齐哈尔市",
                parent_area_key: "230000",
                area_key: "230200",
                rank: "725"
            },
            {
                id: 743,
                name: "鸡西市",
                parent_area_key: "230000",
                area_key: "230300",
                rank: "743"
            },
            {
                id: 754,
                name: "鹤岗市",
                parent_area_key: "230000",
                area_key: "230400",
                rank: "754"
            },
            {
                id: 764,
                name: "双鸭山市",
                parent_area_key: "230000",
                area_key: "230500",
                rank: "764"
            },
            {
                id: 774,
                name: "大庆市",
                parent_area_key: "230000",
                area_key: "230600",
                rank: "774"
            },
            {
                id: 785,
                name: "伊春市",
                parent_area_key: "230000",
                area_key: "230700",
                rank: "785"
            },
            {
                id: 804,
                name: "佳木斯市",
                parent_area_key: "230000",
                area_key: "230800",
                rank: "804"
            },
            {
                id: 816,
                name: "七台河市",
                parent_area_key: "230000",
                area_key: "230900",
                rank: "816"
            },
            {
                id: 822,
                name: "牡丹江市",
                parent_area_key: "230000",
                area_key: "231000",
                rank: "822"
            },
            {
                id: 834,
                name: "黑河市",
                parent_area_key: "230000",
                area_key: "231100",
                rank: "834"
            },
            {
                id: 842,
                name: "绥化市",
                parent_area_key: "230000",
                area_key: "231200",
                rank: "842"
            },
            {
                id: 854,
                name: "大兴安岭地区",
                parent_area_key: "230000",
                area_key: "232700",
                rank: "854"
            }
        ]
    },
    {
        id: 858,
        name: "上海市",
        parent_area_key: "",
        area_key: "310000",
        rank: "858",
        _child: [
            {
                id: 859,
                name: "市辖区",
                parent_area_key: "310000",
                area_key: "310100",
                rank: "859"
            }
        ]
    },
    {
        id: 876,
        name: "江苏省",
        parent_area_key: "",
        area_key: "320000",
        rank: "876",
        _child: [
            {
                id: 877,
                name: "南京市",
                parent_area_key: "320000",
                area_key: "320100",
                rank: "877"
            },
            {
                id: 890,
                name: "无锡市",
                parent_area_key: "320000",
                area_key: "320200",
                rank: "890"
            },
            {
                id: 899,
                name: "徐州市",
                parent_area_key: "320000",
                area_key: "320300",
                rank: "899"
            },
            {
                id: 911,
                name: "常州市",
                parent_area_key: "320000",
                area_key: "320400",
                rank: "911"
            },
            {
                id: 919,
                name: "苏州市",
                parent_area_key: "320000",
                area_key: "320500",
                rank: "919"
            },
            {
                id: 930,
                name: "南通市",
                parent_area_key: "320000",
                area_key: "320600",
                rank: "930"
            },
            {
                id: 940,
                name: "连云港市",
                parent_area_key: "320000",
                area_key: "320700",
                rank: "940"
            },
            {
                id: 948,
                name: "淮安市",
                parent_area_key: "320000",
                area_key: "320800",
                rank: "948"
            },
            {
                id: 957,
                name: "盐城市",
                parent_area_key: "320000",
                area_key: "320900",
                rank: "957"
            },
            {
                id: 968,
                name: "扬州市",
                parent_area_key: "320000",
                area_key: "321000",
                rank: "968"
            },
            {
                id: 976,
                name: "镇江市",
                parent_area_key: "320000",
                area_key: "321100",
                rank: "976"
            },
            {
                id: 984,
                name: "泰州市",
                parent_area_key: "320000",
                area_key: "321200",
                rank: "984"
            },
            {
                id: 992,
                name: "宿迁市",
                parent_area_key: "320000",
                area_key: "321300",
                rank: "992"
            }
        ]
    },
    {
        id: 999,
        name: "浙江省",
        parent_area_key: "",
        area_key: "330000",
        rank: "999",
        _child: [
            {
                id: 1000,
                name: "杭州市",
                parent_area_key: "330000",
                area_key: "330100",
                rank: "1000"
            },
            {
                id: 1015,
                name: "宁波市",
                parent_area_key: "330000",
                area_key: "330200",
                rank: "1015"
            },
            {
                id: 1028,
                name: "温州市",
                parent_area_key: "330000",
                area_key: "330300",
                rank: "1028"
            },
            {
                id: 1041,
                name: "嘉兴市",
                parent_area_key: "330000",
                area_key: "330400",
                rank: "1041"
            },
            {
                id: 1050,
                name: "湖州市",
                parent_area_key: "330000",
                area_key: "330500",
                rank: "1050"
            },
            {
                id: 1057,
                name: "绍兴市",
                parent_area_key: "330000",
                area_key: "330600",
                rank: "1057"
            },
            {
                id: 1065,
                name: "金华市",
                parent_area_key: "330000",
                area_key: "330700",
                rank: "1065"
            },
            {
                id: 1076,
                name: "衢州市",
                parent_area_key: "330000",
                area_key: "330800",
                rank: "1076"
            },
            {
                id: 1084,
                name: "舟山市",
                parent_area_key: "330000",
                area_key: "330900",
                rank: "1084"
            },
            {
                id: 1090,
                name: "台州市",
                parent_area_key: "330000",
                area_key: "331000",
                rank: "1090"
            },
            {
                id: 1101,
                name: "丽水市",
                parent_area_key: "330000",
                area_key: "331100",
                rank: "1101"
            }
        ]
    },
    {
        id: 1112,
        name: "安徽省",
        parent_area_key: "",
        area_key: "340000",
        rank: "1112",
        _child: [
            {
                id: 1113,
                name: "合肥市",
                parent_area_key: "340000",
                area_key: "340100",
                rank: "1113"
            },
            {
                id: 1124,
                name: "芜湖市",
                parent_area_key: "340000",
                area_key: "340200",
                rank: "1124"
            },
            {
                id: 1134,
                name: "蚌埠市",
                parent_area_key: "340000",
                area_key: "340300",
                rank: "1134"
            },
            {
                id: 1143,
                name: "淮南市",
                parent_area_key: "340000",
                area_key: "340400",
                rank: "1143"
            },
            {
                id: 1152,
                name: "马鞍山市",
                parent_area_key: "340000",
                area_key: "340500",
                rank: "1152"
            },
            {
                id: 1160,
                name: "淮北市",
                parent_area_key: "340000",
                area_key: "340600",
                rank: "1160"
            },
            {
                id: 1166,
                name: "铜陵市",
                parent_area_key: "340000",
                area_key: "340700",
                rank: "1166"
            },
            {
                id: 1172,
                name: "安庆市",
                parent_area_key: "340000",
                area_key: "340800",
                rank: "1172"
            },
            {
                id: 1184,
                name: "黄山市",
                parent_area_key: "340000",
                area_key: "341000",
                rank: "1184"
            },
            {
                id: 1193,
                name: "滁州市",
                parent_area_key: "340000",
                area_key: "341100",
                rank: "1193"
            },
            {
                id: 1203,
                name: "阜阳市",
                parent_area_key: "340000",
                area_key: "341200",
                rank: "1203"
            },
            {
                id: 1213,
                name: "宿州市",
                parent_area_key: "340000",
                area_key: "341300",
                rank: "1213"
            },
            {
                id: 1220,
                name: "六安市",
                parent_area_key: "340000",
                area_key: "341500",
                rank: "1220"
            },
            {
                id: 1229,
                name: "亳州市",
                parent_area_key: "340000",
                area_key: "341600",
                rank: "1229"
            },
            {
                id: 1235,
                name: "池州市",
                parent_area_key: "340000",
                area_key: "341700",
                rank: "1235"
            },
            {
                id: 1241,
                name: "宣城市",
                parent_area_key: "340000",
                area_key: "341800",
                rank: "1241"
            }
        ]
    },
    {
        id: 1250,
        name: "福建省",
        parent_area_key: "",
        area_key: "350000",
        rank: "1250",
        _child: [
            {
                id: 1251,
                name: "福州市",
                parent_area_key: "350000",
                area_key: "350100",
                rank: "1251"
            },
            {
                id: 1266,
                name: "厦门市",
                parent_area_key: "350000",
                area_key: "350200",
                rank: "1266"
            },
            {
                id: 1274,
                name: "莆田市",
                parent_area_key: "350000",
                area_key: "350300",
                rank: "1274"
            },
            {
                id: 1281,
                name: "三明市",
                parent_area_key: "350000",
                area_key: "350400",
                rank: "1281"
            },
            {
                id: 1295,
                name: "泉州市",
                parent_area_key: "350000",
                area_key: "350500",
                rank: "1295"
            },
            {
                id: 1309,
                name: "漳州市",
                parent_area_key: "350000",
                area_key: "350600",
                rank: "1309"
            },
            {
                id: 1322,
                name: "南平市",
                parent_area_key: "350000",
                area_key: "350700",
                rank: "1322"
            },
            {
                id: 1334,
                name: "龙岩市",
                parent_area_key: "350000",
                area_key: "350800",
                rank: "1334"
            },
            {
                id: 1343,
                name: "宁德市",
                parent_area_key: "350000",
                area_key: "350900",
                rank: "1343"
            }
        ]
    },
    {
        id: 1354,
        name: "江西省",
        parent_area_key: "",
        area_key: "360000",
        rank: "1354",
        _child: [
            {
                id: 1355,
                name: "南昌市",
                parent_area_key: "360000",
                area_key: "360100",
                rank: "1355"
            },
            {
                id: 1366,
                name: "景德镇市",
                parent_area_key: "360000",
                area_key: "360200",
                rank: "1366"
            },
            {
                id: 1372,
                name: "萍乡市",
                parent_area_key: "360000",
                area_key: "360300",
                rank: "1372"
            },
            {
                id: 1379,
                name: "九江市",
                parent_area_key: "360000",
                area_key: "360400",
                rank: "1379"
            },
            {
                id: 1394,
                name: "新余市",
                parent_area_key: "360000",
                area_key: "360500",
                rank: "1394"
            },
            {
                id: 1398,
                name: "鹰潭市",
                parent_area_key: "360000",
                area_key: "360600",
                rank: "1398"
            },
            {
                id: 1403,
                name: "赣州市",
                parent_area_key: "360000",
                area_key: "360700",
                rank: "1403"
            },
            {
                id: 1423,
                name: "吉安市",
                parent_area_key: "360000",
                area_key: "360800",
                rank: "1423"
            },
            {
                id: 1438,
                name: "宜春市",
                parent_area_key: "360000",
                area_key: "360900",
                rank: "1438"
            },
            {
                id: 1450,
                name: "抚州市",
                parent_area_key: "360000",
                area_key: "361000",
                rank: "1450"
            },
            {
                id: 1463,
                name: "上饶市",
                parent_area_key: "360000",
                area_key: "361100",
                rank: "1463"
            }
        ]
    },
    {
        id: 1477,
        name: "山东省",
        parent_area_key: "",
        area_key: "370000",
        rank: "1477",
        _child: [
            {
                id: 1478,
                name: "济南市",
                parent_area_key: "370000",
                area_key: "370100",
                rank: "1478"
            },
            {
                id: 1490,
                name: "青岛市",
                parent_area_key: "370000",
                area_key: "370200",
                rank: "1490"
            },
            {
                id: 1502,
                name: "淄博市",
                parent_area_key: "370000",
                area_key: "370300",
                rank: "1502"
            },
            {
                id: 1512,
                name: "枣庄市",
                parent_area_key: "370000",
                area_key: "370400",
                rank: "1512"
            },
            {
                id: 1520,
                name: "东营市",
                parent_area_key: "370000",
                area_key: "370500",
                rank: "1520"
            },
            {
                id: 1527,
                name: "烟台市",
                parent_area_key: "370000",
                area_key: "370600",
                rank: "1527"
            },
            {
                id: 1541,
                name: "潍坊市",
                parent_area_key: "370000",
                area_key: "370700",
                rank: "1541"
            },
            {
                id: 1555,
                name: "济宁市",
                parent_area_key: "370000",
                area_key: "370800",
                rank: "1555"
            },
            {
                id: 1568,
                name: "泰安市",
                parent_area_key: "370000",
                area_key: "370900",
                rank: "1568"
            },
            {
                id: 1576,
                name: "威海市",
                parent_area_key: "370000",
                area_key: "371000",
                rank: "1576"
            },
            {
                id: 1582,
                name: "日照市",
                parent_area_key: "370000",
                area_key: "371100",
                rank: "1582"
            },
            {
                id: 1588,
                name: "莱芜市",
                parent_area_key: "370000",
                area_key: "371200",
                rank: "1588"
            },
            {
                id: 1592,
                name: "临沂市",
                parent_area_key: "370000",
                area_key: "371300",
                rank: "1592"
            },
            {
                id: 1606,
                name: "德州市",
                parent_area_key: "370000",
                area_key: "371400",
                rank: "1606"
            },
            {
                id: 1619,
                name: "聊城市",
                parent_area_key: "370000",
                area_key: "371500",
                rank: "1619"
            },
            {
                id: 1629,
                name: "滨州市",
                parent_area_key: "370000",
                area_key: "371600",
                rank: "1629"
            },
            {
                id: 1638,
                name: "菏泽市",
                parent_area_key: "370000",
                area_key: "371700",
                rank: "1638"
            }
        ]
    },
    {
        id: 1649,
        name: "河南省",
        parent_area_key: "",
        area_key: "410000",
        rank: "1649",
        _child: [
            {
                id: 1650,
                name: "郑州市",
                parent_area_key: "410000",
                area_key: "410100",
                rank: "1650"
            },
            {
                id: 1664,
                name: "开封市",
                parent_area_key: "410000",
                area_key: "410200",
                rank: "1664"
            },
            {
                id: 1676,
                name: "洛阳市",
                parent_area_key: "410000",
                area_key: "410300",
                rank: "1676"
            },
            {
                id: 1693,
                name: "平顶山市",
                parent_area_key: "410000",
                area_key: "410400",
                rank: "1693"
            },
            {
                id: 1705,
                name: "安阳市",
                parent_area_key: "410000",
                area_key: "410500",
                rank: "1705"
            },
            {
                id: 1716,
                name: "鹤壁市",
                parent_area_key: "410000",
                area_key: "410600",
                rank: "1716"
            },
            {
                id: 1723,
                name: "新乡市",
                parent_area_key: "410000",
                area_key: "410700",
                rank: "1723"
            },
            {
                id: 1737,
                name: "焦作市",
                parent_area_key: "410000",
                area_key: "410800",
                rank: "1737"
            },
            {
                id: 1749,
                name: "濮阳市",
                parent_area_key: "410000",
                area_key: "410900",
                rank: "1749"
            },
            {
                id: 1757,
                name: "许昌市",
                parent_area_key: "410000",
                area_key: "411000",
                rank: "1757"
            },
            {
                id: 1765,
                name: "漯河市",
                parent_area_key: "410000",
                area_key: "411100",
                rank: "1765"
            },
            {
                id: 1772,
                name: "三门峡市",
                parent_area_key: "410000",
                area_key: "411200",
                rank: "1772"
            },
            {
                id: 1780,
                name: "南阳市",
                parent_area_key: "410000",
                area_key: "411300",
                rank: "1780"
            },
            {
                id: 1795,
                name: "商丘市",
                parent_area_key: "410000",
                area_key: "411400",
                rank: "1795"
            },
            {
                id: 1806,
                name: "信阳市",
                parent_area_key: "410000",
                area_key: "411500",
                rank: "1806"
            },
            {
                id: 1818,
                name: "周口市",
                parent_area_key: "410000",
                area_key: "411600",
                rank: "1818"
            },
            {
                id: 1830,
                name: "驻马店市",
                parent_area_key: "410000",
                area_key: "411700",
                rank: "1830"
            },
            {
                id: 1842,
                name: "省直辖县级行政区划",
                parent_area_key: "410000",
                area_key: "419000",
                rank: "1842"
            }
        ]
    },
    {
        id: 1844,
        name: "湖北省",
        parent_area_key: "",
        area_key: "420000",
        rank: "1844",
        _child: [
            {
                id: 1845,
                name: "武汉市",
                parent_area_key: "420000",
                area_key: "420100",
                rank: "1845"
            },
            {
                id: 1860,
                name: "黄石市",
                parent_area_key: "420000",
                area_key: "420200",
                rank: "1860"
            },
            {
                id: 1868,
                name: "十堰市",
                parent_area_key: "420000",
                area_key: "420300",
                rank: "1868"
            },
            {
                id: 1878,
                name: "宜昌市",
                parent_area_key: "420000",
                area_key: "420500",
                rank: "1878"
            },
            {
                id: 1893,
                name: "襄阳市",
                parent_area_key: "420000",
                area_key: "420600",
                rank: "1893"
            },
            {
                id: 1904,
                name: "鄂州市",
                parent_area_key: "420000",
                area_key: "420700",
                rank: "1904"
            },
            {
                id: 1909,
                name: "荆门市",
                parent_area_key: "420000",
                area_key: "420800",
                rank: "1909"
            },
            {
                id: 1916,
                name: "孝感市",
                parent_area_key: "420000",
                area_key: "420900",
                rank: "1916"
            },
            {
                id: 1925,
                name: "荆州市",
                parent_area_key: "420000",
                area_key: "421000",
                rank: "1925"
            },
            {
                id: 1935,
                name: "黄冈市",
                parent_area_key: "420000",
                area_key: "421100",
                rank: "1935"
            },
            {
                id: 1947,
                name: "咸宁市",
                parent_area_key: "420000",
                area_key: "421200",
                rank: "1947"
            },
            {
                id: 1955,
                name: "随州市",
                parent_area_key: "420000",
                area_key: "421300",
                rank: "1955"
            },
            {
                id: 1960,
                name: "恩施土家族苗族自治州",
                parent_area_key: "420000",
                area_key: "422800",
                rank: "1960"
            },
            {
                id: 1969,
                name: "省直辖县级行政区划",
                parent_area_key: "420000",
                area_key: "429000",
                rank: "1969"
            }
        ]
    },
    {
        id: 1974,
        name: "湖南省",
        parent_area_key: "",
        area_key: "430000",
        rank: "1974",
        _child: [
            {
                id: 1975,
                name: "长沙市",
                parent_area_key: "430000",
                area_key: "430100",
                rank: "1975"
            },
            {
                id: 1986,
                name: "株洲市",
                parent_area_key: "430000",
                area_key: "430200",
                rank: "1986"
            },
            {
                id: 1997,
                name: "湘潭市",
                parent_area_key: "430000",
                area_key: "430300",
                rank: "1997"
            },
            {
                id: 2004,
                name: "衡阳市",
                parent_area_key: "430000",
                area_key: "430400",
                rank: "2004"
            },
            {
                id: 2018,
                name: "邵阳市",
                parent_area_key: "430000",
                area_key: "430500",
                rank: "2018"
            },
            {
                id: 2032,
                name: "岳阳市",
                parent_area_key: "430000",
                area_key: "430600",
                rank: "2032"
            },
            {
                id: 2043,
                name: "常德市",
                parent_area_key: "430000",
                area_key: "430700",
                rank: "2043"
            },
            {
                id: 2054,
                name: "张家界市",
                parent_area_key: "430000",
                area_key: "430800",
                rank: "2054"
            },
            {
                id: 2060,
                name: "益阳市",
                parent_area_key: "430000",
                area_key: "430900",
                rank: "2060"
            },
            {
                id: 2068,
                name: "郴州市",
                parent_area_key: "430000",
                area_key: "431000",
                rank: "2068"
            },
            {
                id: 2081,
                name: "永州市",
                parent_area_key: "430000",
                area_key: "431100",
                rank: "2081"
            },
            {
                id: 2094,
                name: "怀化市",
                parent_area_key: "430000",
                area_key: "431200",
                rank: "2094"
            },
            {
                id: 2108,
                name: "娄底市",
                parent_area_key: "430000",
                area_key: "431300",
                rank: "2108"
            },
            {
                id: 2115,
                name: "湘西土家族苗族自治州",
                parent_area_key: "430000",
                area_key: "433100",
                rank: "2115"
            }
        ]
    },
    {
        id: 2124,
        name: "广东省",
        parent_area_key: "",
        area_key: "440000",
        rank: "2124",
        _child: [
            {
                id: 2125,
                name: "广州市",
                parent_area_key: "440000",
                area_key: "440100",
                rank: "2125"
            },
            {
                id: 2138,
                name: "韶关市",
                parent_area_key: "440000",
                area_key: "440200",
                rank: "2138"
            },
            {
                id: 2150,
                name: "深圳市",
                parent_area_key: "440000",
                area_key: "440300",
                rank: "2150"
            },
            {
                id: 2158,
                name: "珠海市",
                parent_area_key: "440000",
                area_key: "440400",
                rank: "2158"
            },
            {
                id: 2163,
                name: "汕头市",
                parent_area_key: "440000",
                area_key: "440500",
                rank: "2163"
            },
            {
                id: 2172,
                name: "佛山市",
                parent_area_key: "440000",
                area_key: "440600",
                rank: "2172"
            },
            {
                id: 2179,
                name: "江门市",
                parent_area_key: "440000",
                area_key: "440700",
                rank: "2179"
            },
            {
                id: 2188,
                name: "湛江市",
                parent_area_key: "440000",
                area_key: "440800",
                rank: "2188"
            },
            {
                id: 2199,
                name: "茂名市",
                parent_area_key: "440000",
                area_key: "440900",
                rank: "2199"
            },
            {
                id: 2206,
                name: "肇庆市",
                parent_area_key: "440000",
                area_key: "441200",
                rank: "2206"
            },
            {
                id: 2216,
                name: "惠州市",
                parent_area_key: "440000",
                area_key: "441300",
                rank: "2216"
            },
            {
                id: 2223,
                name: "梅州市",
                parent_area_key: "440000",
                area_key: "441400",
                rank: "2223"
            },
            {
                id: 2233,
                name: "汕尾市",
                parent_area_key: "440000",
                area_key: "441500",
                rank: "2233"
            },
            {
                id: 2239,
                name: "河源市",
                parent_area_key: "440000",
                area_key: "441600",
                rank: "2239"
            },
            {
                id: 2247,
                name: "阳江市",
                parent_area_key: "440000",
                area_key: "441700",
                rank: "2247"
            },
            {
                id: 2253,
                name: "清远市",
                parent_area_key: "440000",
                area_key: "441800",
                rank: "2253"
            },
            {
                id: 2263,
                name: "东莞市",
                parent_area_key: "440000",
                area_key: "441900",
                rank: "2263"
            },
            {
                id: 2264,
                name: "中山市",
                parent_area_key: "440000",
                area_key: "442000",
                rank: "2264"
            },
            {
                id: 2265,
                name: "潮州市",
                parent_area_key: "440000",
                area_key: "445100",
                rank: "2265"
            },
            {
                id: 2270,
                name: "揭阳市",
                parent_area_key: "440000",
                area_key: "445200",
                rank: "2270"
            },
            {
                id: 2277,
                name: "云浮市",
                parent_area_key: "440000",
                area_key: "445300",
                rank: "2277"
            }
        ]
    },
    {
        id: 2284,
        name: "广西壮族自治区",
        parent_area_key: "",
        area_key: "450000",
        rank: "2284",
        _child: [
            {
                id: 2285,
                name: "南宁市",
                parent_area_key: "450000",
                area_key: "450100",
                rank: "2285"
            },
            {
                id: 2299,
                name: "柳州市",
                parent_area_key: "450000",
                area_key: "450200",
                rank: "2299"
            },
            {
                id: 2311,
                name: "桂林市",
                parent_area_key: "450000",
                area_key: "450300",
                rank: "2311"
            },
            {
                id: 2330,
                name: "梧州市",
                parent_area_key: "450000",
                area_key: "450400",
                rank: "2330"
            },
            {
                id: 2339,
                name: "北海市",
                parent_area_key: "450000",
                area_key: "450500",
                rank: "2339"
            },
            {
                id: 2345,
                name: "防城港市",
                parent_area_key: "450000",
                area_key: "450600",
                rank: "2345"
            },
            {
                id: 2351,
                name: "钦州市",
                parent_area_key: "450000",
                area_key: "450700",
                rank: "2351"
            },
            {
                id: 2357,
                name: "贵港市",
                parent_area_key: "450000",
                area_key: "450800",
                rank: "2357"
            },
            {
                id: 2364,
                name: "玉林市",
                parent_area_key: "450000",
                area_key: "450900",
                rank: "2364"
            },
            {
                id: 2373,
                name: "百色市",
                parent_area_key: "450000",
                area_key: "451000",
                rank: "2373"
            },
            {
                id: 2387,
                name: "贺州市",
                parent_area_key: "450000",
                area_key: "451100",
                rank: "2387"
            },
            {
                id: 2394,
                name: "河池市",
                parent_area_key: "450000",
                area_key: "451200",
                rank: "2394"
            },
            {
                id: 2407,
                name: "来宾市",
                parent_area_key: "450000",
                area_key: "451300",
                rank: "2407"
            },
            {
                id: 2415,
                name: "崇左市",
                parent_area_key: "450000",
                area_key: "451400",
                rank: "2415"
            }
        ]
    },
    {
        id: 2424,
        name: "海南省",
        parent_area_key: "",
        area_key: "460000",
        rank: "2424",
        _child: [
            {
                id: 2425,
                name: "海口市",
                parent_area_key: "460000",
                area_key: "460100",
                rank: "2425"
            },
            {
                id: 2431,
                name: "三亚市",
                parent_area_key: "460000",
                area_key: "460200",
                rank: "2431"
            },
            {
                id: 2437,
                name: "三沙市",
                parent_area_key: "460000",
                area_key: "460300",
                rank: "2437"
            },
            {
                id: 2438,
                name: "儋州市",
                parent_area_key: "460000",
                area_key: "460400",
                rank: "2438"
            },
            {
                id: 2439,
                name: "省直辖县级行政区划",
                parent_area_key: "460000",
                area_key: "469000",
                rank: "2439"
            }
        ]
    },
    {
        id: 2455,
        name: "重庆市",
        parent_area_key: "",
        area_key: "500000",
        rank: "2455",
        _child: [
            {
                id: 2456,
                name: "市辖区",
                parent_area_key: "500000",
                area_key: "500100",
                rank: "2456"
            },
            {
                id: 2481,
                name: "县",
                parent_area_key: "500000",
                area_key: "500200",
                rank: "2481"
            }
        ]
    },
    {
        id: 2496,
        name: "四川省",
        parent_area_key: "",
        area_key: "510000",
        rank: "2496",
        _child: [
            {
                id: 2497,
                name: "成都市",
                parent_area_key: "510000",
                area_key: "510100",
                rank: "2497"
            },
            {
                id: 2519,
                name: "自贡市",
                parent_area_key: "510000",
                area_key: "510300",
                rank: "2519"
            },
            {
                id: 2527,
                name: "攀枝花市",
                parent_area_key: "510000",
                area_key: "510400",
                rank: "2527"
            },
            {
                id: 2534,
                name: "泸州市",
                parent_area_key: "510000",
                area_key: "510500",
                rank: "2534"
            },
            {
                id: 2543,
                name: "德阳市",
                parent_area_key: "510000",
                area_key: "510600",
                rank: "2543"
            },
            {
                id: 2551,
                name: "绵阳市",
                parent_area_key: "510000",
                area_key: "510700",
                rank: "2551"
            },
            {
                id: 2562,
                name: "广元市",
                parent_area_key: "510000",
                area_key: "510800",
                rank: "2562"
            },
            {
                id: 2571,
                name: "遂宁市",
                parent_area_key: "510000",
                area_key: "510900",
                rank: "2571"
            },
            {
                id: 2578,
                name: "内江市",
                parent_area_key: "510000",
                area_key: "511000",
                rank: "2578"
            },
            {
                id: 2585,
                name: "乐山市",
                parent_area_key: "510000",
                area_key: "511100",
                rank: "2585"
            },
            {
                id: 2598,
                name: "南充市",
                parent_area_key: "510000",
                area_key: "511300",
                rank: "2598"
            },
            {
                id: 2609,
                name: "眉山市",
                parent_area_key: "510000",
                area_key: "511400",
                rank: "2609"
            },
            {
                id: 2617,
                name: "宜宾市",
                parent_area_key: "510000",
                area_key: "511500",
                rank: "2617"
            },
            {
                id: 2629,
                name: "广安市",
                parent_area_key: "510000",
                area_key: "511600",
                rank: "2629"
            },
            {
                id: 2637,
                name: "达州市",
                parent_area_key: "510000",
                area_key: "511700",
                rank: "2637"
            },
            {
                id: 2646,
                name: "雅安市",
                parent_area_key: "510000",
                area_key: "511800",
                rank: "2646"
            },
            {
                id: 2656,
                name: "巴中市",
                parent_area_key: "510000",
                area_key: "511900",
                rank: "2656"
            },
            {
                id: 2663,
                name: "资阳市",
                parent_area_key: "510000",
                area_key: "512000",
                rank: "2663"
            },
            {
                id: 2668,
                name: "阿坝藏族羌族自治州",
                parent_area_key: "510000",
                area_key: "513200",
                rank: "2668"
            },
            {
                id: 2682,
                name: "甘孜藏族自治州",
                parent_area_key: "510000",
                area_key: "513300",
                rank: "2682"
            },
            {
                id: 2701,
                name: "凉山彝族自治州",
                parent_area_key: "510000",
                area_key: "513400",
                rank: "2701"
            }
        ]
    },
    {
        id: 2719,
        name: "贵州省",
        parent_area_key: "",
        area_key: "520000",
        rank: "2719",
        _child: [
            {
                id: 2720,
                name: "贵阳市",
                parent_area_key: "520000",
                area_key: "520100",
                rank: "2720"
            },
            {
                id: 2732,
                name: "六盘水市",
                parent_area_key: "520000",
                area_key: "520200",
                rank: "2732"
            },
            {
                id: 2737,
                name: "遵义市",
                parent_area_key: "520000",
                area_key: "520300",
                rank: "2737"
            },
            {
                id: 2753,
                name: "安顺市",
                parent_area_key: "520000",
                area_key: "520400",
                rank: "2753"
            },
            {
                id: 2761,
                name: "毕节市",
                parent_area_key: "520000",
                area_key: "520500",
                rank: "2761"
            },
            {
                id: 2771,
                name: "铜仁市",
                parent_area_key: "520000",
                area_key: "520600",
                rank: "2771"
            },
            {
                id: 2783,
                name: "黔西南布依族苗族自治州",
                parent_area_key: "520000",
                area_key: "522300",
                rank: "2783"
            },
            {
                id: 2792,
                name: "黔东南苗族侗族自治州",
                parent_area_key: "520000",
                area_key: "522600",
                rank: "2792"
            },
            {
                id: 2809,
                name: "黔南布依族苗族自治州",
                parent_area_key: "520000",
                area_key: "522700",
                rank: "2809"
            }
        ]
    },
    {
        id: 2822,
        name: "云南省",
        parent_area_key: "",
        area_key: "530000",
        rank: "2822",
        _child: [
            {
                id: 2823,
                name: "昆明市",
                parent_area_key: "530000",
                area_key: "530100",
                rank: "2823"
            },
            {
                id: 2839,
                name: "曲靖市",
                parent_area_key: "530000",
                area_key: "530300",
                rank: "2839"
            },
            {
                id: 2850,
                name: "玉溪市",
                parent_area_key: "530000",
                area_key: "530400",
                rank: "2850"
            },
            {
                id: 2861,
                name: "保山市",
                parent_area_key: "530000",
                area_key: "530500",
                rank: "2861"
            },
            {
                id: 2868,
                name: "昭通市",
                parent_area_key: "530000",
                area_key: "530600",
                rank: "2868"
            },
            {
                id: 2881,
                name: "丽江市",
                parent_area_key: "530000",
                area_key: "530700",
                rank: "2881"
            },
            {
                id: 2888,
                name: "普洱市",
                parent_area_key: "530000",
                area_key: "530800",
                rank: "2888"
            },
            {
                id: 2900,
                name: "临沧市",
                parent_area_key: "530000",
                area_key: "530900",
                rank: "2900"
            },
            {
                id: 2910,
                name: "楚雄彝族自治州",
                parent_area_key: "530000",
                area_key: "532300",
                rank: "2910"
            },
            {
                id: 2921,
                name: "红河哈尼族彝族自治州",
                parent_area_key: "530000",
                area_key: "532500",
                rank: "2921"
            },
            {
                id: 2935,
                name: "文山壮族苗族自治州",
                parent_area_key: "530000",
                area_key: "532600",
                rank: "2935"
            },
            {
                id: 2944,
                name: "西双版纳傣族自治州",
                parent_area_key: "530000",
                area_key: "532800",
                rank: "2944"
            },
            {
                id: 2948,
                name: "大理白族自治州",
                parent_area_key: "530000",
                area_key: "532900",
                rank: "2948"
            },
            {
                id: 2961,
                name: "德宏傣族景颇族自治州",
                parent_area_key: "530000",
                area_key: "533100",
                rank: "2961"
            },
            {
                id: 2967,
                name: "怒江傈僳族自治州",
                parent_area_key: "530000",
                area_key: "533300",
                rank: "2967"
            },
            {
                id: 2972,
                name: "迪庆藏族自治州",
                parent_area_key: "530000",
                area_key: "533400",
                rank: "2972"
            }
        ]
    },
    {
        id: 2976,
        name: "西藏自治区",
        parent_area_key: "",
        area_key: "540000",
        rank: "2976",
        _child: [
            {
                id: 2977,
                name: "拉萨市",
                parent_area_key: "540000",
                area_key: "540100",
                rank: "2977"
            },
            {
                id: 2987,
                name: "日喀则市",
                parent_area_key: "540000",
                area_key: "540200",
                rank: "2987"
            },
            {
                id: 3006,
                name: "昌都市",
                parent_area_key: "540000",
                area_key: "540300",
                rank: "3006"
            },
            {
                id: 3018,
                name: "林芝市",
                parent_area_key: "540000",
                area_key: "540400",
                rank: "3018"
            },
            {
                id: 3026,
                name: "山南市",
                parent_area_key: "540000",
                area_key: "540500",
                rank: "3026"
            },
            {
                id: 3040,
                name: "那曲地区",
                parent_area_key: "540000",
                area_key: "542400",
                rank: "3040"
            },
            {
                id: 3052,
                name: "阿里地区",
                parent_area_key: "540000",
                area_key: "542500",
                rank: "3052"
            }
        ]
    },
    {
        id: 3060,
        name: "陕西省",
        parent_area_key: "",
        area_key: "610000",
        rank: "3060",
        _child: [
            {
                id: 3061,
                name: "西安市",
                parent_area_key: "610000",
                area_key: "610100",
                rank: "3061"
            },
            {
                id: 3076,
                name: "铜川市",
                parent_area_key: "610000",
                area_key: "610200",
                rank: "3076"
            },
            {
                id: 3082,
                name: "宝鸡市",
                parent_area_key: "610000",
                area_key: "610300",
                rank: "3082"
            },
            {
                id: 3096,
                name: "咸阳市",
                parent_area_key: "610000",
                area_key: "610400",
                rank: "3096"
            },
            {
                id: 3112,
                name: "渭南市",
                parent_area_key: "610000",
                area_key: "610500",
                rank: "3112"
            },
            {
                id: 3125,
                name: "延安市",
                parent_area_key: "610000",
                area_key: "610600",
                rank: "3125"
            },
            {
                id: 3140,
                name: "汉中市",
                parent_area_key: "610000",
                area_key: "610700",
                rank: "3140"
            },
            {
                id: 3153,
                name: "榆林市",
                parent_area_key: "610000",
                area_key: "610800",
                rank: "3153"
            },
            {
                id: 3167,
                name: "安康市",
                parent_area_key: "610000",
                area_key: "610900",
                rank: "3167"
            },
            {
                id: 3179,
                name: "商洛市",
                parent_area_key: "610000",
                area_key: "611000",
                rank: "3179"
            }
        ]
    },
    {
        id: 3188,
        name: "甘肃省",
        parent_area_key: "",
        area_key: "620000",
        rank: "3188",
        _child: [
            {
                id: 3189,
                name: "兰州市",
                parent_area_key: "620000",
                area_key: "620100",
                rank: "3189"
            },
            {
                id: 3199,
                name: "嘉峪关市",
                parent_area_key: "620000",
                area_key: "620200",
                rank: "3199"
            },
            {
                id: 3201,
                name: "金昌市",
                parent_area_key: "620000",
                area_key: "620300",
                rank: "3201"
            },
            {
                id: 3205,
                name: "白银市",
                parent_area_key: "620000",
                area_key: "620400",
                rank: "3205"
            },
            {
                id: 3212,
                name: "天水市",
                parent_area_key: "620000",
                area_key: "620500",
                rank: "3212"
            },
            {
                id: 3221,
                name: "武威市",
                parent_area_key: "620000",
                area_key: "620600",
                rank: "3221"
            },
            {
                id: 3227,
                name: "张掖市",
                parent_area_key: "620000",
                area_key: "620700",
                rank: "3227"
            },
            {
                id: 3235,
                name: "平凉市",
                parent_area_key: "620000",
                area_key: "620800",
                rank: "3235"
            },
            {
                id: 3244,
                name: "酒泉市",
                parent_area_key: "620000",
                area_key: "620900",
                rank: "3244"
            },
            {
                id: 3253,
                name: "庆阳市",
                parent_area_key: "620000",
                area_key: "621000",
                rank: "3253"
            },
            {
                id: 3263,
                name: "定西市",
                parent_area_key: "620000",
                area_key: "621100",
                rank: "3263"
            },
            {
                id: 3272,
                name: "陇南市",
                parent_area_key: "620000",
                area_key: "621200",
                rank: "3272"
            },
            {
                id: 3283,
                name: "临夏回族自治州",
                parent_area_key: "620000",
                area_key: "622900",
                rank: "3283"
            },
            {
                id: 3292,
                name: "甘南藏族自治州",
                parent_area_key: "620000",
                area_key: "623000",
                rank: "3292"
            }
        ]
    },
    {
        id: 3301,
        name: "青海省",
        parent_area_key: "",
        area_key: "630000",
        rank: "3301",
        _child: [
            {
                id: 3302,
                name: "西宁市",
                parent_area_key: "630000",
                area_key: "630100",
                rank: "3302"
            },
            {
                id: 3311,
                name: "海东市",
                parent_area_key: "630000",
                area_key: "630200",
                rank: "3311"
            },
            {
                id: 3318,
                name: "海北藏族自治州",
                parent_area_key: "630000",
                area_key: "632200",
                rank: "3318"
            },
            {
                id: 3323,
                name: "黄南藏族自治州",
                parent_area_key: "630000",
                area_key: "632300",
                rank: "3323"
            },
            {
                id: 3328,
                name: "海南藏族自治州",
                parent_area_key: "630000",
                area_key: "632500",
                rank: "3328"
            },
            {
                id: 3334,
                name: "果洛藏族自治州",
                parent_area_key: "630000",
                area_key: "632600",
                rank: "3334"
            },
            {
                id: 3341,
                name: "玉树藏族自治州",
                parent_area_key: "630000",
                area_key: "632700",
                rank: "3341"
            },
            {
                id: 3348,
                name: "海西蒙古族藏族自治州",
                parent_area_key: "630000",
                area_key: "632800",
                rank: "3348"
            }
        ]
    },
    {
        id: 3354,
        name: "宁夏回族自治区",
        parent_area_key: "",
        area_key: "640000",
        rank: "3354",
        _child: [
            {
                id: 3355,
                name: "银川市",
                parent_area_key: "640000",
                area_key: "640100",
                rank: "3355"
            },
            {
                id: 3363,
                name: "石嘴山市",
                parent_area_key: "640000",
                area_key: "640200",
                rank: "3363"
            },
            {
                id: 3368,
                name: "吴忠市",
                parent_area_key: "640000",
                area_key: "640300",
                rank: "3368"
            },
            {
                id: 3375,
                name: "固原市",
                parent_area_key: "640000",
                area_key: "640400",
                rank: "3375"
            },
            {
                id: 3382,
                name: "中卫市",
                parent_area_key: "640000",
                area_key: "640500",
                rank: "3382"
            }
        ]
    },
    {
        id: 3387,
        name: "新疆维吾尔自治区",
        parent_area_key: "",
        area_key: "650000",
        rank: "3387",
        _child: [
            {
                id: 3388,
                name: "乌鲁木齐市",
                parent_area_key: "650000",
                area_key: "650100",
                rank: "3388"
            },
            {
                id: 3398,
                name: "克拉玛依市",
                parent_area_key: "650000",
                area_key: "650200",
                rank: "3398"
            },
            {
                id: 3404,
                name: "吐鲁番市",
                parent_area_key: "650000",
                area_key: "650400",
                rank: "3404"
            },
            {
                id: 3408,
                name: "哈密市",
                parent_area_key: "650000",
                area_key: "650500",
                rank: "3408"
            },
            {
                id: 3412,
                name: "昌吉回族自治州",
                parent_area_key: "650000",
                area_key: "652300",
                rank: "3412"
            },
            {
                id: 3420,
                name: "博尔塔拉蒙古自治州",
                parent_area_key: "650000",
                area_key: "652700",
                rank: "3420"
            },
            {
                id: 3425,
                name: "巴音郭楞蒙古自治州",
                parent_area_key: "650000",
                area_key: "652800",
                rank: "3425"
            },
            {
                id: 3435,
                name: "阿克苏地区",
                parent_area_key: "650000",
                area_key: "652900",
                rank: "3435"
            },
            {
                id: 3445,
                name: "克孜勒苏柯尔克孜自治州",
                parent_area_key: "650000",
                area_key: "653000",
                rank: "3445"
            },
            {
                id: 3450,
                name: "喀什地区",
                parent_area_key: "650000",
                area_key: "653100",
                rank: "3450"
            },
            {
                id: 3463,
                name: "和田地区",
                parent_area_key: "650000",
                area_key: "653200",
                rank: "3463"
            },
            {
                id: 3472,
                name: "伊犁哈萨克自治州",
                parent_area_key: "650000",
                area_key: "654000",
                rank: "3472"
            },
            {
                id: 3484,
                name: "塔城地区",
                parent_area_key: "650000",
                area_key: "654200",
                rank: "3484"
            },
            {
                id: 3492,
                name: "阿勒泰地区",
                parent_area_key: "650000",
                area_key: "654300",
                rank: "3492"
            },
            {
                id: 3500,
                name: "自治区直辖县级行政区划",
                parent_area_key: "650000",
                area_key: "659000",
                rank: "3500"
            }
        ]
    },
    {
        id: 3506,
        name: "台湾省",
        parent_area_key: "",
        area_key: "710000",
        rank: "3506"
    },
    {
        id: 3507,
        name: "香港特别行政区",
        parent_area_key: "",
        area_key: "810000",
        rank: "3507"
    },
    {
        id: 3508,
        name: "澳门特别行政区",
        parent_area_key: "",
        area_key: "820000",
        rank: "3508"
    }
]